# Lightweight server

This is a lightweight server for static content written in go. It accepts the following environment variables:
* READ_TIMEOUT: read timeout for the http server, specified in Go's duration format: 10s, 10ms, 10m and so on
* WRITE_TIMEOUT: write timeout for the http server, specified in Go's duration format: 10s, 10ms, 10m and so on
* PUBLISH: the directory that will be published on the root of the server. Default to `public`
* PORT: port for the server to listen at. Default 8080

The default working directory of the server is at `/home/server`. Example for serving the content at `/home/server/www` on port 8081:
```
docker run -it -v /host/directory:/home/server/www -e PUBLISH=www -e PORT=8081 -p 8081:8081 matipan/static-server:latest
```

## Use it for your static site docker image
If you want to create a docker image where you build and serve your static site you can do what [this template](https://github.com/matipan/openfaas-hugo-template) does and use this image as runtime:
```dockerfile
FROM <your base image> AS build

# Build your static site here and leave the contents
# on some directory, for example /home/app/public

FROM matipan/static-server:0.1.0 AS runtime

WORKDIR /home/server

# Copy the files from the build image
COPY --from=build /home/app/public public

# Specify the directory you just copied as the
# one that should be served by the server
ENV PUBLISH="public"
# Specify the port for the server to listen at
ENV PORT=8080
# Configure the read timeout for your server, by
# default it's 3 seconds
ENV READ_TIMEOUT=3s
# Configure the write timeout for your server, by
# default it's 3 seconds
ENV WRITE_TIMEOUT=3s

# The entrypoint is inheritated from the static server
# image and will make use of the environment variables
# we just defined
```